export default ({ env }) => ({
  upload: {
    config: {
      breakpoints: {
        xlarge: 2000,
        large: 1000,
        medium: 750,
        small: 500
      }
    }
  }
})