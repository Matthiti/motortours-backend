# Motorritme Backend

This is the backend of https://motorritme.nl

## Requirements

- node 18
- npm

## Install

```sh
npm install
```

## Run

```sh
npm run develop
```

## Build admin panel

```sh
NODE_ENV=production npm run build
```
